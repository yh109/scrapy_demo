# -*- coding: utf-8 -*-
import scrapy
from scrapy.http import Request
from bs4 import BeautifulSoup
from test_f.items import TestFItem
import math
class LuopanSpider(scrapy.Spider):
    name = 'luopan'
    # allowed_domains = ['https://m.fang.com/']
    start_urls = ['http://newhouse.nc.fang.com/']
    baseurl = 'http://newhouse.nc.fang.com/house/s/b9'
    show = False
    def parse(self, response):
        # print(response)
        soup = BeautifulSoup(response.text, 'lxml')
        try:
            page_box = soup.find_all('div', class_="page")[0]
            matches = page_box.ul.li.strong.string
            total_page = math.ceil(int(matches) / 20)
        except Exception as e:
            total_page = 1
        for i in range(total_page):
            url = self.baseurl + str(i + 1)
            yield Request(url, callback=self.get_name)
        pass

    def get_name(self, response):
        soup = BeautifulSoup(response.text, 'lxml')
        # print(soup)
        house_elements = soup.find_all('div', class_="nlc_details")
        for house_elem in house_elements:
            try:
                url = house_elem.div.div.a['href']
                yield Request(url, callback=self.get_info)
            except Exception as e:
                print(e)
    def get_info(self, response):
        soup = BeautifulSoup(response.text, 'lxml')
        if not self.show:
            print(soup)
            self.show = True