# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class TestFItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    #名字
    name = scrapy.Field()
    #地址
    address = scrapy.Field()
    #网页链接
    url = scrapy.Field()

    pass
